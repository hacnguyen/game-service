CREATE DATABASE pokemon;
\c pokemon
CREATE SEQUENCE user_infos_id_seq;
CREATE TABLE user_infos (
	id serial primary key,
	user_name text unique
);

CREATE SEQUENCE scores_id_seq;

CREATE TABLE scores (
	id serial PRIMARY KEY,
    id_user int,
	score int,
    level_game int,
    FOREIGN KEY(id_user) 
	  REFERENCES user_infos(id)
);