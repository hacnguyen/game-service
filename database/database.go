package database

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
	//for sql
	_ "github.com/lib/pq"
)

var db *gorm.DB

//GetEnv ...
func GetEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

//ConvertStringToInt ...
func ConvertStringToInt(str string) (int, error) {
	res, err := strconv.Atoi(str)
	return res, err
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "123465"
	dbname   = "pokemon"
)

//Init ...
func Init() {

	dbinfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	var err error
	fmt.Println(dbinfo)
	db, err = ConnectDB(dbinfo)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Success")
}

//ConnectDB ...
func ConnectDB(dbinfo string) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", dbinfo)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return db, nil
}

//GetDB ...
func GetDB() *gorm.DB {
	return db
}

//CloseDB before close application
func CloseDB() {
	db.Close()
	db = nil
}
