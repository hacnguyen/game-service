package app

import (
	"log"

	controller "../controller"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

//SetupRouter ...
func SetupRouter() *gin.Engine {
	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	router.Use(cors.New(config))
	router.GET("/", func(c *gin.Context) {
		c.File("index.html")
	})

	router.POST("/user", controller.PostUser)

	router.POST("/update-score-level", controller.UpdateScoreLevel)

	router.GET("/score-level", controller.GetScore)

	router.GET("/score-total", controller.GetTotalScore)

	//for authentication
	//authMiddleware := middleware.InitJWTMiddleware()
	// router.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
	// 	c.JSON(http.StatusNotFound, gin.H{"message": "Page not found"})
	// })
	return router
}

//InitRouter App Handler Start From Here
func InitRouter() {
	router := SetupRouter()
	if err := router.Run(":7007"); err != nil {
		log.Fatal(err)
	}
}
