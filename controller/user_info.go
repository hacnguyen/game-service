package controller

import (
	"fmt"
	"strings"

	"../model"
	"github.com/gin-gonic/gin"
)

// PostUser : create user
func PostUser(c *gin.Context) {
	userInfo := model.UserInfo{}
	c.ShouldBind(&userInfo)
	//

	if userInfo.UserName == "" {
		c.JSON(400, gin.H{
			"error": "user name can not be null",
		})
		return
	}

	_userInfo, err := model.PostUser(userInfo)
	if err != nil {

		errorString := fmt.Sprintf(err.Error())

		if strings.Contains(errorString, "duplicate") {
			c.JSON(500, gin.H{
				"error": "username already exist",
			})
		} else {
			c.JSON(500, gin.H{
				"error": "can't creat user",
			})
		}

	} else {
		c.JSON(200, _userInfo)
	}

}
