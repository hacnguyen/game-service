package controller

import (
	"strconv"

	"../model"
	"github.com/gin-gonic/gin"
)

// ScoreInterface ...
type ScoreInterface struct {
	UserID    string `json:"id_user"`
	Score     []int  `json:"score"`
	LevelGame []int  `json:"level"`
}

// UpdateScoreLevel ...
func UpdateScoreLevel(c *gin.Context) {
	scoreInterface := ScoreInterface{}
	c.ShouldBind(&scoreInterface)
	userID, _ := strconv.Atoi(scoreInterface.UserID)
	err := model.UpdateScoreGameLevel(userID, scoreInterface.LevelGame, scoreInterface.Score)
	if err == nil {
		c.JSON(200, gin.H{
			"message": "update score success",
		})
	} else {
		c.JSON(500, gin.H{
			"error": "update score fail",
		})
	}
}

// GetScore ...
func GetScore(c *gin.Context) {
	userID := c.Query("id-user")
	level := c.Query("lv")
	userIDInt, _ := strconv.Atoi(userID)
	levelInt, _ := strconv.Atoi(level)
	result, err := model.GetScore(userIDInt, levelInt)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "can't get score",
		})

	} else {
		c.JSON(200, result)
	}

}

// GetTotalScore ...
func GetTotalScore(c *gin.Context) {
	userID := c.Query("id-user")
	userIDInt, _ := strconv.Atoi(userID)
	result, err := model.GetTotalScore(userIDInt)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "can't get score",
		})

	} else {
		c.JSON(200, result)
	}
}
