package main

import (
	"./app"
	"./database"
)

func main() {
	defer database.CloseDB()
	database.Init()
	app.InitRouter()
}
