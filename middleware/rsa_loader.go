package middleware

import (
	"crypto/rsa"
	"io/ioutil"
	"log"

	jwt "github.com/dgrijalva/jwt-go"
)

// location of the files used for signing and verification
const (
	privKeyPath = "app/keys/app.rsa"     // openssl genrsa -out app.rsa keysize
	pubKeyPath  = "app/keys/app.rsa.pub" // openssl rsa -in app.rsa -pubout > app.rsa.pub
)

// keys are held in global variables
// i havn't seen a memory corruption/info leakage in go yet
// but maybe it's a better idea, just to store the public key in ram?
// and load the signKey on every signing request? depends on  your usage i guess
var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

//GetRSAPublicKeyFile ...
func GetRSAPublicKeyFile() string {
	return pubKeyPath
}

//GetRSAPrivateKeyFile ...
func GetRSAPrivateKeyFile() string {
	return privKeyPath
}

//GetRSAPublicKey ...
func GetRSAPublicKey() *rsa.PublicKey {
	return verifyKey
}

//GetRSAPrivateKey ...
func GetRSAPrivateKey() *rsa.PrivateKey {
	return signKey
}

//LoadRSAKeys read the key files before starting http handlers
func LoadRSAKeys() {
	signBytes, err := ioutil.ReadFile(privKeyPath)
	if nil != err {
		log.Fatal(err)
	}

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if nil != err {
		log.Fatal(err)
	}

	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	if nil != err {
		log.Fatal(err)
	}

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if nil != err {
		log.Fatal(err)
	}
}
