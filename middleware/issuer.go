package middleware

import (
	"errors"
	"fmt"
	"log"

	"github.com/dgrijalva/jwt-go"
)

//IssueToken ...
func IssueToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ss, err := token.SignedString(signKey)

	if nil != err {
		log.Println("Error while signing the token")
		log.Printf("Error signing token: %v\n", err)
		return ss, err
	}

	return ss, nil
}

//VerificationToken ...
func VerificationToken(tk string) bool {
	// Parse the token
	token, err := jwt.Parse(tk, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})

	if err != nil {
		fmt.Println("JWT TOKEN NOT OK ", token, token.Header)
		log.Fatal(err)
		return false
	}
	return true
}

//VerificationRefreshToken ...
func VerificationRefreshToken(tokenString string) (string, string, error) {
	type UmbalaUserInfo struct {
		ID    string `json:"id"`
		Email string `json:"email"`
		jwt.StandardClaims
	}
	// Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &UmbalaUserInfo{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	if err != nil {
		return "", "", err
	}

	claims, ok := token.Claims.(*UmbalaUserInfo)

	if !ok {
		return "", "", errors.New("invalid token")
	}
	return claims.ID, claims.Email, err
}
