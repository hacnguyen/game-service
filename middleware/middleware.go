package middleware

import (
	"fmt"
	"log"
	"os"
	"time"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

var identityKey = "id"

type claimsInfo struct {
	Issuer string
	ID     string
}

//InitJWTMiddleware Config Middleware In Here
func InitJWTMiddleware() *jwt.GinJWTMiddleware {
	// Load security keys
	LoadRSAKeys()
	// the jwt middleware
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:            "pokemon-backend",
		Key:              []byte(os.Getenv("secretkey")),
		Timeout:          0,
		MaxRefresh:       0,
		IdentityKey:      identityKey,
		SigningAlgorithm: "RS256",

		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &claimsInfo{
				Issuer: claims["iss"].(string),
				ID:     claims["id"].(string),
			}
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(*claimsInfo); ok && v.Issuer == "WEBSHOP" {
				claims := jwt.ExtractClaims(c)
				if claims["id"].(string) == v.ID {
					return true
				}
				return false
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			fmt.Println(c.Request.Header.Get("Authorization"))
			c.JSON(code, gin.H{
				//"code":    code,
				"message": message,
			})
		},
		TokenLookup: "header: Authorization, query: token, cookie: jwt",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "WEBSHOP",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,

		PrivKeyFile: GetRSAPrivateKeyFile(),

		PubKeyFile: GetRSAPublicKeyFile(),
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	return authMiddleware
}
