package middleware

import (
	jwt "github.com/dgrijalva/jwt-go"
)

//ValidateToken ...
func ValidateToken(tokenString string) (string, error) {
	type UmbalaUserInfo struct {
		ID    string `json:"id"`
		Email string `json:"email"`
		jwt.StandardClaims
	}
	// Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &UmbalaUserInfo{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	claims := token.Claims.(*UmbalaUserInfo)
	//fmt.Println(claims.ID)
	return claims.Email, err
}

//GetIDFromToken ...
func GetIDFromToken(tokenString string) (string, error) {
	type UmbalaUserInfo struct {
		ID    string `json:"id"`
		Email string `json:"email"`
		jwt.StandardClaims
	}
	// Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &UmbalaUserInfo{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	claims := token.Claims.(*UmbalaUserInfo)
	//fmt.Println(claims.ID)
	return claims.ID, err
}
