package model

import (
	"fmt"

	"../database"

	//for sql
	_ "github.com/lib/pq"
)

// UserInfo ...
type UserInfo struct {
	ID       string `json:"id,omitempty"`
	UserName string `json:"user_name,omitempty" gorm:"text"`
}

// PostUser ...
func PostUser(userInfo UserInfo) (UserInfo, error) {
	_userInfo := userInfo
	_userInfo.ID = ""

	err := _userInfo.create()
	if err != nil {
		fmt.Println("can't create user info ", err)
		return UserInfo{}, err
	}
	return _userInfo, nil
}

func (userInfo *UserInfo) create() error {
	db := database.GetDB()

	res := db.Create(&userInfo)

	if res.Error != nil {
		return res.Error
	}
	return nil
}

func getUser(userID int) (UserInfo, error) {
	var user UserInfo
	db := database.GetDB()
	res := db.Where("id = ?", userID).Find(&user)
	if res.Error != nil {
		err := fmt.Errorf("User is unavailable")
		return user, err
	}
	return user, nil
}

func getAllUser() ([]UserInfo, error) {
	var users []UserInfo
	db := database.GetDB()
	res := db.Find(&users)
	if res.Error != nil {
		err := fmt.Errorf("User is unavailable")
		return users, err
	}
	return users, nil
}
