package model

import (
	"fmt"
	"sort"
	"strconv"

	"../database"
)

// Score ...
type Score struct {
	ID        string `json:"id,omitempty"`
	IDUser    int    `json:"id_user,omitempty" gorm:"text"`
	Score     int    `json:"score,omitempty" gorm:"integer" form:"score"`
	LevelGame int    `json:"level_game,omitempty" gorm:"integer" form:"level_game"`
}

// GetScoreInterface ...
type GetScoreInterface struct {
	Score  int       `json:"score,omitempty"`
	Rank   int       `json:"rank,omitempty"`
	Names  [3]string `json:"names,omitempty"`
	Scores [3]int    `json:"scores,omitempty"`
}

func (score *Score) create() error {
	db := database.GetDB()

	res := db.Create(&score)

	if res.Error != nil {
		return res.Error
	}
	return nil
}

func getScore(userID int, level int) (Score, error) {
	var score Score
	db := database.GetDB()
	res := db.Where("id_user = ? AND level_game = ?", userID, level).Find(&score)
	if res.Error != nil {
		err := fmt.Errorf("Score is unavailable")
		return score, err
	}
	return score, nil
}

// UpdateScoreGameLevel ...
func UpdateScoreGameLevel(userID int, levels []int, score []int) error {
	var _errResult error = nil
	for i := 0; i < len(levels); i++ {
		_level := levels[i]
		_scoreInt := score[i]
		_score, err := getScore(userID, _level)
		if err != nil {
			var _newScore = Score{}
			_newScore.LevelGame = _level
			_newScore.IDUser = userID
			_newScore.Score = _scoreInt
			err = _newScore.create()
			if err != nil {
				_errResult = err
			}

		} else {
			var _newScore = Score{}
			_newScore.LevelGame = _score.LevelGame
			_newScore.IDUser = _score.IDUser
			_newScore.Score = _scoreInt
			_newScore.ID = _score.ID

			err = _score.updateScore(_newScore)
			if err != nil {
				_errResult = err
			}
		}
	}
	return _errResult
}

//UpdateScore ...
func (score *Score) updateScore(temp Score) error {
	return score.updateAllField(temp)
}

//Update all field of userinfo table
func (score *Score) updateAllField(newScore Score) error {
	db := database.GetDB()
	res := db.Model(&score).Where("id = ?", score.ID).Updates(newScore)
	if res.Error != nil {
		//log.Fatal(res.Error)
		return res.Error
	}
	return nil
}

// GetScore ...
func GetScore(userID int, level int) (GetScoreInterface, error) {
	var result = GetScoreInterface{}
	score, err := getScore(userID, level)
	result.Score = score.Score
	scores, _ := getAllScoreOfLevel(level)

	sort.SliceStable(scores, func(i, j int) bool {
		return scores[i].Score > scores[j].Score
	})
	count := 3
	if len(scores) < 3 {
		count = len(scores)
	}

	var _names [3]string
	var _scores [3]int

	for i := 0; i < count; i++ {
		_scores[i] = scores[i].Score
		user, _ := getUser(scores[i].IDUser)
		_names[i] = user.UserName
	}

	result.Scores = _scores
	result.Names = _names

	var rank = 1

	for i := 0; i < len(scores); i++ {
		if result.Score == scores[i].Score {
			rank = i + 1
			break
		}
	}

	result.Rank = rank

	return result, err

}

func getAllScoreOfLevel(level int) ([]Score, error) {
	var scores []Score
	db := database.GetDB()
	res := db.Where("level_game = ?", level).Find(&scores)
	if res.Error != nil {
		err := fmt.Errorf("Score is unavailable")
		return scores, err
	}
	return scores, nil
}

func getTotalScoreOfUser(userID int) int {
	var scores []Score
	db := database.GetDB()
	db.Where("id_user = ?", userID).Find(&scores)
	var total = 0
	for i := 0; i < len(scores); i++ {
		total += scores[i].Score
	}
	return total
}

// GetTotalScore ...
func GetTotalScore(userID int) (GetScoreInterface, error) {
	var result = GetScoreInterface{}
	result.Score = getTotalScoreOfUser(userID)

	//	result.Score = score.Score
	users, _ := getAllUser()

	type ScoreItemt struct {
		name  string
		score int
	}

	var _scoresItemt []ScoreItemt

	for i := 0; i < len(users); i++ {
		user := users[i]
		_userID, _ := strconv.Atoi(user.ID)
		_total := getTotalScoreOfUser(_userID)
		_temp := ScoreItemt{}
		_temp.name = user.UserName
		_temp.score = _total
		_scoresItemt = append(_scoresItemt, _temp)
	}

	sort.SliceStable(_scoresItemt, func(i, j int) bool {
		return _scoresItemt[i].score > _scoresItemt[j].score
	})

	count := 3
	if len(_scoresItemt) < 3 {
		count = len(_scoresItemt)
	}

	for i := 0; i < count; i++ {
		result.Names[i] = _scoresItemt[i].name
		result.Scores[i] = _scoresItemt[i].score

	}

	rank := 1

	for i := 0; i < len(_scoresItemt); i++ {
		if result.Score == _scoresItemt[i].score {
			rank = i + 1
			break
		}
	}

	result.Rank = rank

	return result, nil
}
